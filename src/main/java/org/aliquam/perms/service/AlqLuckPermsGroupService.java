package org.aliquam.perms.service;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.node.types.DisplayNameNode;
import net.luckperms.api.node.types.WeightNode;
import net.luckperms.api.query.QueryOptions;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class AlqLuckPermsGroupService extends AlqLuckPermsService {

    public AlqLuckPermsGroupService(LuckPerms luckperms, AlqLuckPermsMapper mapper) {
        super(luckperms, mapper);
    }

    public List<AlqPermGroup> listGroups() {
        groupManager.loadAllGroups().join();
        Set<Group> groups = groupManager.getLoadedGroups();
        return groups.stream().map(mapper::map).collect(Collectors.toList());
    }

    public Optional<AlqPermGroup> getGroup(String name) {
        return groupManager.loadGroup(name).join()
                .map(mapper::map);
    }

    public void putGroup(AlqPermGroup alqGroup) {
        Group group = groupManager.loadGroup(alqGroup.getName()).join()
                .orElseGet(() -> groupManager.createAndLoadGroup(alqGroup.getName()).join());
        if(alqGroup.getDisplayName() != null) {
            addNode(group, DisplayNameNode.builder().displayName(alqGroup.getDisplayName()));
        }
        if(alqGroup.getWeight() != null) {
            addNode(group, WeightNode.builder().weight(alqGroup.getWeight()));
        }
        groupManager.saveGroup(group);
    }

    public void deleteGroup(String name) {
        groupManager.loadGroup(name).join()
                .ifPresent(e -> groupManager.deleteGroup(e).join());
    }

    public List<AlqPermGroup> getInheritedGroups(String groupName, AlqPermContext context) {
        Group group = loadGroupOrThrow(groupName);
        QueryOptions queryOptions = withContext(group.getQueryOptions(), context);
        return group.getInheritedGroups(queryOptions).stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    public void addInheritedGroup(String groupName, ModifyInheritedGroupRequest inheritedGroupRequest) {
        Group group = loadGroupOrThrow(groupName);
        loadGroupOrThrow(inheritedGroupRequest.getInheritedGroupName());
        addNode(group, mapper.map(inheritedGroupRequest));
        groupManager.saveGroup(group);
    }

    public void removeInheritedGroup(String groupName, ModifyInheritedGroupRequest inheritedGroupRequest) {
        Group group = loadGroupOrThrow(groupName);
        loadGroupOrThrow(inheritedGroupRequest.getInheritedGroupName());
        removeNode(group, mapper.map(inheritedGroupRequest));
        groupManager.saveGroup(group);
    }

//    public List<AlqPermUser> getUsers(AlqPermGroup alqGroup) {
//    }

    public List<AlqPerm> listPermissions(String groupName) {
        Group group = loadGroupOrThrow(groupName);
        group.getNodes(NodeType.PERMISSION);
        return mapper.map(group.getNodes(NodeType.PERMISSION));
    }

    public void addPermission(String groupName, AlqPerm permission) {
        Group group = loadGroupOrThrow(groupName);
        addNode(group, mapper.map(permission));
        groupManager.saveGroup(group);
    }

    public void removePermission(String groupName, AlqPerm permission) {
        Group group = loadGroupOrThrow(groupName);
        removeNode(group, mapper.map(permission));
        groupManager.saveGroup(group);
    }

}
