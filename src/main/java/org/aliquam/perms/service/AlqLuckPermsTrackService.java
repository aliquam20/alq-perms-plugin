package org.aliquam.perms.service;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.track.Track;
import org.aliquam.perms.api.model.AlqPermTrack;
import spark.Spark;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class AlqLuckPermsTrackService extends AlqLuckPermsService {


    public AlqLuckPermsTrackService(LuckPerms luckperms, AlqLuckPermsMapper mapper) {
        super(luckperms, mapper);
    }

    public List<AlqPermTrack> listTracks() {
        trackManager.loadAllTracks().join();
        Set<Track> tracks = trackManager.getLoadedTracks();
        return tracks.stream().map(mapper::map).collect(Collectors.toList());
    }

    public Optional<AlqPermTrack> getTrack(String name) {
        return trackManager.loadTrack(name).join()
                .map(mapper::map);
    }

    public void putTrack(AlqPermTrack alqTrack) {
        Track track = trackManager.loadTrack(alqTrack.getName()).join()
                .orElseGet(() -> trackManager.createAndLoadTrack(alqTrack.getName()).join());
        List<Group> groups = alqTrack.getGroups().stream()
                .map(g -> groupManager.loadGroup(g).join()
                        .orElseThrow(() -> new RuntimeException("Cannot load group: " + g)))
                .collect(Collectors.toList());

        track.clearGroups();
        groups.forEach(track::appendGroup);
        trackManager.saveTrack(track);
    }

    public void deleteTrack(String name) {
        trackManager.loadTrack(name).join()
                .ifPresent(e -> trackManager.deleteTrack(e).join());
    }

    public void promote(UUID userId, String currentGroupName, String trackName) {
        User user = loadUser(userId);
        Track track = loadTrackOrThrow(trackName);
        Group currentGroup = loadGroupOrThrow(currentGroupName);
        String nextGroupName = track.getNext(currentGroup);
        if(nextGroupName == null) {
            throw new RuntimeException("Cannot promote - User is already at the top of the track"); // TODO: Status 200?
        }
        if(!isInGroup(user, currentGroupName)) {
            throw new RuntimeException("Promotion aborted because of unexpected user group (expected: " + currentGroupName + ")"); // TODO: Status 400?
        }

        track.promote(user, ImmutableContextSet.empty());

        if(!isInGroup(user, nextGroupName)) {
            throw new RuntimeException("Promotion aborted because next group is different than expected(" + nextGroupName + ")");
        }

        userManager.saveUser(user);
    }

    public void demote(UUID userId, String currentGroupName, String trackName) {
        User user = loadUser(userId);
        Track track = loadTrackOrThrow(trackName);
        Group currentGroup = loadGroupOrThrow(currentGroupName);
        String previousGroupName = track.getPrevious(currentGroup);
        if(previousGroupName == null) {
            throw new RuntimeException("Cannot deomote - User is already at the bottom of the track"); // TODO: Status 200?
        }
        if(!isInGroup(user, currentGroupName)) {
            throw new RuntimeException("Demotion aborted because of unexpected user group (expected: " + currentGroupName + ")"); // TODO: Status 400?
        }

        track.demote(user, ImmutableContextSet.empty());

        if(!isInGroup(user, previousGroupName)) {
            throw new RuntimeException("Demotion aborted because previous group is different than expected(" + previousGroupName + ")");
        }

        userManager.saveUser(user);
    }

}
