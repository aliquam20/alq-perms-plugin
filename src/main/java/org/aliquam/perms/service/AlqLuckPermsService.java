package org.aliquam.perms.service;

import lombok.experimental.UtilityClass;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.context.MutableContextSet;
import net.luckperms.api.model.PermissionHolder;
import net.luckperms.api.model.data.DataMutateResult;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.group.GroupManager;
import net.luckperms.api.model.user.User;
import net.luckperms.api.model.user.UserManager;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeBuilder;
import net.luckperms.api.query.QueryOptions;
import net.luckperms.api.track.Track;
import net.luckperms.api.track.TrackManager;
import org.aliquam.perms.api.model.AlqPermContext;

import java.util.UUID;
import java.util.stream.Collectors;

public class AlqLuckPermsService {
    protected final UserManager userManager;
    protected final GroupManager groupManager;
    protected final TrackManager trackManager;
    protected final AlqLuckPermsMapper mapper;

    public AlqLuckPermsService(LuckPerms luckperms, AlqLuckPermsMapper mapper) {
        userManager = luckperms.getUserManager();
        groupManager = luckperms.getGroupManager();
        trackManager = luckperms.getTrackManager();
        this.mapper = mapper;
    }

    public void addNode(PermissionHolder permHolder, NodeBuilder<?, ?> nodeBuilder) {
        addNode(permHolder, nodeBuilder.build());
    }

    public void addNode(PermissionHolder permHolder, Node node) {
        permHolder.data().remove(node);
        DataMutateResult result = permHolder.data().add(node);
        if(!result.wasSuccessful()) {
            throw new RuntimeException("Mutation failed: " + result);
        }
    }

    public void removeNode(PermissionHolder permHolder, NodeBuilder<?, ?> nodeBuilder) {
        removeNode(permHolder, nodeBuilder.build());
    }

    public void removeNode(PermissionHolder permHolder, Node node) {
        DataMutateResult result = permHolder.data().remove(node);
        if(!result.wasSuccessful()) {
            throw new RuntimeException("Mutation failed: " + result);
        }
    }

    public User loadUser(UUID id) {
        return userManager.loadUser(id).join();
    }

    public Group loadGroupOrThrow(String name) {
        return groupManager.loadGroup(name).join()
                .orElseThrow(() -> new RuntimeException("Group '" + name + "' not found"));
    }

    public Track loadTrackOrThrow(String name) {
        return trackManager.loadTrack(name).join()
                .orElseThrow(() -> new RuntimeException("Track '" + name + "' not found"));
    }

    public boolean isInGroup(User user, String group) {
        return user.getInheritedGroups(user.getQueryOptions()).stream()
                .map(mapper::map)
                .anyMatch(g -> g.getName().equalsIgnoreCase(group));
    }

    public QueryOptions withContext(QueryOptions queryOptions, AlqPermContext context) {
        MutableContextSet newContext = queryOptions.context().mutableCopy();
        if(context.getServer() != null) {
            newContext.removeAll("server");
            newContext.add("server", context.getServer());
        }
        if(context.getWorld() != null) {
            newContext.removeAll("world");
            newContext.add("world", context.getWorld());
        }
        return queryOptions.toBuilder().context(newContext).build();
    }



}
