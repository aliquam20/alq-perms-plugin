package org.aliquam.perms.service;

import net.luckperms.api.context.ContextSet;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeBuilder;
import net.luckperms.api.node.types.InheritanceNode;
import net.luckperms.api.node.types.PermissionNode;
import net.luckperms.api.track.Track;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermTrack;
import org.aliquam.perms.api.model.AlqPermUser;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;
import java.util.OptionalInt;
import java.util.UUID;

@Mapper
public abstract class AlqLuckPermsMapper {

    public abstract AlqPermGroup map(Group group);

    @Mapping(target = "id", source = "uniqueId")
    public abstract AlqPermUser map(User user);

    @Mapping(target = "id", source = ".")
    public abstract AlqPermUser map(UUID id);

    public abstract AlqPermTrack map(Track track);

    public AlqPermContext map(ContextSet contextSet) {
        AlqPermContext alqContext = new AlqPermContext();
        alqContext.setServer(contextSet.getAnyValue("server").orElse(null));
        alqContext.setWorld(contextSet.getAnyValue("world").orElse(null));
        return alqContext;
    }

    public AlqPerm map(PermissionNode permission) {
        AlqPerm ret = new AlqPerm();
        ret.setPermission(permission.getPermission());
        ret.setContext(map(permission.getContexts()));
        return ret;
    }

    public abstract List<AlqPerm> map(Collection<PermissionNode> permission);

    public Node map(AlqPerm permission) {
        NodeBuilder<?, ?> builder = Node.builder(permission.getPermission());
        addContext(builder, permission.getContext());
        return builder.build();
    }

    public Node map(ModifyInheritedGroupRequest request) {
        NodeBuilder<?, ?> builder = InheritanceNode.builder(request.getInheritedGroupName());
        addContext(builder, request.getContext());
        return builder.build();
    }

    public Integer map(OptionalInt value) {
        if(value.isEmpty()) {
            return null;
        }
        return value.getAsInt();
    }

    private void addContext(NodeBuilder<?, ?> builder, AlqPermContext context) {
        if(context.getServer() != null) {
            builder.withContext("server", context.getServer());
        }
        if(context.getWorld() != null) {
            builder.withContext("world", context.getWorld());
        }
    }

}
