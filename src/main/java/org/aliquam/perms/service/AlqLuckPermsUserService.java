package org.aliquam.perms.service;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedPermissionData;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.query.QueryOptions;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.AlqPermUser;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class AlqLuckPermsUserService extends AlqLuckPermsService {

    public AlqLuckPermsUserService(LuckPerms luckperms, AlqLuckPermsMapper mapper) {
        super(luckperms, mapper);
    }

    public List<AlqPermUser> listUsers() {
        Set<UUID> users = userManager.getUniqueUsers().join();
        return users.stream().map(mapper::map).collect(Collectors.toList());
    }

    public AlqPermUser getUser(UUID id) {
        return mapper.map(userManager.loadUser(id).join());
    }

    public void deleteUser(UUID id) {
//        userManager.loadUser(id).join().deleteAllPermsAndGroups()
        User user = userManager.loadUser(id).join();
        user.data().clear();
        userManager.saveUser(user).join();
        userManager.deletePlayerData(id).join();
    }

    public List<AlqPermGroup> getInheritedGroups(UUID id, AlqPermContext context) {
        User user = userManager.loadUser(id).join();
        QueryOptions queryOptions = withContext(user.getQueryOptions(), context);
        return user.getInheritedGroups(queryOptions).stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    public void addInheritedGroup(UUID id, ModifyInheritedGroupRequest inheritedGroupRequest) {
        User user = userManager.loadUser(id).join();
        loadGroupOrThrow(inheritedGroupRequest.getInheritedGroupName());
        addNode(user, mapper.map(inheritedGroupRequest));
        userManager.saveUser(user);
    }

    public void removeInheritedGroup(UUID id, ModifyInheritedGroupRequest inheritedGroupRequest) {
        User user = userManager.loadUser(id).join();
        loadGroupOrThrow(inheritedGroupRequest.getInheritedGroupName());
        removeNode(user, mapper.map(inheritedGroupRequest));
        userManager.saveUser(user);
    }

    public List<AlqPerm> listPermissions(UUID id) {
        User user = userManager.loadUser(id).join();
        return mapper.map(user.getNodes(NodeType.PERMISSION));
    }

    public void addPermission(UUID id, AlqPerm permission) {
        User user = userManager.loadUser(id).join();
        addNode(user, mapper.map(permission));
        userManager.saveUser(user);
    }

    public void removePermission(UUID id, AlqPerm permission) {
        User user = userManager.loadUser(id).join();
        removeNode(user, mapper.map(permission));
        userManager.saveUser(user);
    }

    public boolean hasPermission(UUID id, String permission, AlqPermContext context) {
        User user = userManager.loadUser(id).join();
        QueryOptions queryOptions = withContext(user.getQueryOptions(), context);
        CachedPermissionData permissions = user.getCachedData().getPermissionData(queryOptions);
        return permissions.checkPermission(permission).asBoolean();
    }



}
