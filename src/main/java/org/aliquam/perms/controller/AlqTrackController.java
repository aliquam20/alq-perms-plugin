package org.aliquam.perms.controller;

import org.aliquam.cum.AlqJson;
import org.aliquam.perms.api.model.AlqPermTrack;
import org.aliquam.perms.api.model.AlqPermTrackMoteUserRequest;
import org.aliquam.perms.service.AlqLuckPermsTrackService;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.Objects;

public class AlqTrackController extends AlqController {
    private final AlqLuckPermsTrackService service;

    public AlqTrackController(AlqLuckPermsTrackService service) {
        this.service = service;
    }

    public Object listTracks(Request request, Response response) {
        return service.listTracks();
    }

    public Object getTrack(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        return service.getTrack(name)
                .orElseThrow(() -> Spark.halt(404, "Requested track '" + name + "' not found"));
    }

    public Object putTrack(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPermTrack alqTrack = getBodyOrHalt(request, AlqPermTrack.class);
        if(!Objects.equals(name, alqTrack.getName())) {
            return Spark.halt(400, "names in URL(" + name + ") and body (" + alqTrack.getName() + ") don't match");
        }
        service.putTrack(alqTrack);
        return alqTrack;
    }

    public Object deleteTrack(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        service.deleteTrack(name);
        return null;
    }

    public Object promote(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPermTrackMoteUserRequest moteRequest = getBodyOrHalt(request, AlqPermTrackMoteUserRequest.class);
        service.promote(moteRequest.getUser(), moteRequest.getCurrentGroup(), name);
        return null;
    }

    public Object demote(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPermTrackMoteUserRequest moteRequest = getBodyOrHalt(request, AlqPermTrackMoteUserRequest.class);
        service.demote(moteRequest.getUser(), moteRequest.getCurrentGroup(), name);
        return null;
    }

}