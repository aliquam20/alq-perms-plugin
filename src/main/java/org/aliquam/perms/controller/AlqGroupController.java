package org.aliquam.perms.controller;

import org.aliquam.cum.AlqJson;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.AlqPermGroup;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;
import org.aliquam.perms.service.AlqLuckPermsGroupService;
import spark.Request;
import spark.Response;
import spark.Spark;

import java.util.Objects;

public class AlqGroupController extends AlqController {
    private final AlqLuckPermsGroupService service;

    public AlqGroupController(AlqLuckPermsGroupService service) {
        this.service = service;
    }

    public Object listGroups(Request request, Response response) {
        return service.listGroups();
    }

    public Object getGroup(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        return service.getGroup(name)
                .orElseThrow(() -> Spark.halt(404, "Requested group '" + name + "' not found"));
    }

    public Object putGroup(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPermGroup alqGroup = getBodyOrHalt(request, AlqPermGroup.class);
        if(!Objects.equals(name, alqGroup.getName())) {
            return Spark.halt(400, "names in URL(" + name + ") and body (" + alqGroup.getName() + ") don't match");
        }
        service.putGroup(alqGroup);
        return alqGroup;
    }

    public Object deleteGroup(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        service.deleteGroup(name);
        return null;
    }

    public Object getInheritedGroups(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPermContext context = readContextFromQueryParams(request);
        return service.getInheritedGroups(name, context);
    }

    public Object addInheritedGroup(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        ModifyInheritedGroupRequest inheritedGroupRequest = getBodyOrHalt(request, ModifyInheritedGroupRequest.class);
        service.addInheritedGroup(name, inheritedGroupRequest);
        return null;
    }

    public Object removeInheritedGroup(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        ModifyInheritedGroupRequest inheritedGroupRequest = getBodyOrHalt(request, ModifyInheritedGroupRequest.class);
        service.removeInheritedGroup(name, inheritedGroupRequest);
        return null;
    }

    public Object listPermissions(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        return service.listPermissions(name);
    }

    public Object addPermission(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPerm perm = getBodyOrHalt(request, AlqPerm.class);
        service.addPermission(name, perm);
        return null;
    }

    public Object removePermission(Request request, Response response) {
        String name = getParamOrHalt(request, "name");
        AlqPerm perm = getBodyOrHalt(request, AlqPerm.class);
        service.removePermission(name, perm);
        return null;
    }

}