package org.aliquam.perms.controller;

import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.RequestAuthenticationException;
import org.aliquam.session.api.RequestAuthenticatorService;
import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

public class RequireSession implements Filter {

    private final RequestAuthenticatorService requestAuthenticatorService;

    public RequireSession(RequestAuthenticatorService requestAuthenticatorService) {
        this.requestAuthenticatorService = requestAuthenticatorService;
    }

    @Override
    public void handle(Request request, Response response) throws Exception {
        String sessionHeader = request.headers(AlqSessionApi.SESSION_HEADER);
        try {
            requestAuthenticatorService.checkSession(sessionHeader, true);
        } catch (RequestAuthenticationException e) {
            Spark.halt(e.getErrorHttpStatus(), e.getMessage());
        }
    }
}
