package org.aliquam.perms.controller;

import org.aliquam.cum.AlqJson;
import org.aliquam.perms.api.model.AlqPerm;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;
import org.aliquam.perms.service.AlqLuckPermsUserService;
import spark.Request;
import spark.Response;

import java.util.UUID;

public class AlqUserController extends AlqController {
    private final AlqLuckPermsUserService service;

    public AlqUserController(AlqLuckPermsUserService service) {
        this.service = service;
    }

    public Object listUsers(Request request, Response response) {
        return service.listUsers();
    }

    public Object getUser(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        return service.getUser(id);
    }

    public Object deleteUser(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        service.deleteUser(id);
        return null;
    }

    public Object getInheritedGroups(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        AlqPermContext context = readContextFromQueryParams(request);
        return service.getInheritedGroups(id, context);
    }

    public Object addInheritedGroup(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        ModifyInheritedGroupRequest inheritedGroupRequest = getBodyOrHalt(request, ModifyInheritedGroupRequest.class);
        service.addInheritedGroup(id, inheritedGroupRequest);
        return null;
    }

    public Object removeInheritedGroup(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        ModifyInheritedGroupRequest inheritedGroupRequest = getBodyOrHalt(request, ModifyInheritedGroupRequest.class);
        service.removeInheritedGroup(id, inheritedGroupRequest);
        return null;
    }

    public Object listPermissions(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        return service.listPermissions(id);
    }

    public Object addPermission(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        AlqPerm perm = getBodyOrHalt(request, AlqPerm.class);
        service.addPermission(id, perm);
        return null;
    }

    public Object removePermission(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        AlqPerm perm = getBodyOrHalt(request, AlqPerm.class);
        service.removePermission(id, perm);
        return null;
    }

    public Object hasPermission(Request request, Response response) {
        UUID id = getParamUuidOrHalt(request, "id");
        String permission = request.params("permission");
//        String permission = request.queryParams("permission");
        AlqPermContext context = readContextFromQueryParams(request);
        return service.hasPermission(id, permission, context);
    }

}