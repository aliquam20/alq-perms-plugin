package org.aliquam.perms.controller;

import org.aliquam.cum.AlqJson;
import org.aliquam.perms.api.model.AlqPermContext;
import org.aliquam.perms.api.model.ModifyInheritedGroupRequest;
import spark.HaltException;
import spark.Request;
import spark.Spark;

import java.util.UUID;

public class AlqController {

    protected AlqPermContext readContextFromQueryParams(Request request) {
        AlqPermContext context = new AlqPermContext();
        context.setServer(request.queryParams("server"));
        context.setWorld(request.queryParams("world"));
        return context;
    }

    protected UUID getParamUuidOrHalt(Request request, String key) {
        String idStr = getParamOrHalt(request, key);
        try {
            return UUID.fromString(idStr);
        } catch (IllegalArgumentException ex) {
            throw Spark.halt(400, key + " UUID is malformed; Provided: " + idStr);
        }
    }

    protected String getParamOrHalt(Request request, String key) {
        String value = request.params(key);
        if(value == null) {
            Spark.halt(400, key + " parameter cannot be null");
        }
        return value;
    }

    protected <T> T getBodyOrHalt(Request request, Class<T> responseType) {
        try {
            String body = request.body();
            if(body == null) {
                throw Spark.halt(400, "Request body is missing");
            }
            return AlqJson.parse(body, responseType);
        } catch (Exception ex) {
            if(ex instanceof HaltException) {
                throw ex;
            }
            throw Spark.halt(400, "Request body is malformed");
        }
    }

}
