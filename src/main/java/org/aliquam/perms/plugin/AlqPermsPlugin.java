package org.aliquam.perms.plugin;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.luckperms.api.LuckPerms;
import org.aliquam.cum.AlqJson;
import org.aliquam.perms.controller.AlqGroupController;
import org.aliquam.perms.controller.AlqTrackController;
import org.aliquam.perms.controller.AlqUserController;
import org.aliquam.perms.controller.RequireSession;
import org.aliquam.perms.service.AlqLuckPermsGroupService;
import org.aliquam.perms.service.AlqLuckPermsMapper;
import org.aliquam.perms.service.AlqLuckPermsTrackService;
import org.aliquam.perms.service.AlqLuckPermsUserService;
import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.RequestAuthenticatorService;
import org.aliquam.session.api.connector.AlqSessionConnector;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.mapstruct.factory.Mappers;
import spark.Spark;

import java.util.Optional;

@SuppressWarnings("unused") // Instantiated by Minecraft server
@Slf4j
public class AlqPermsPlugin extends JavaPlugin {

    @SneakyThrows
    @Override
    public void onEnable() {
        Spark.port(8080);

        LuckPerms luckperms = Optional.ofNullable(Bukkit.getServicesManager().getRegistration(LuckPerms.class))
                .map(RegisteredServiceProvider::getProvider)
                .orElseThrow(() -> new IllegalStateException("LuckPerms has NOT been initialized!"));
        AlqLuckPermsMapper luckpermsMapper = Mappers.getMapper(AlqLuckPermsMapper.class);

        Spark.before((req, res) -> log.info("Received request: " + req.uri()));

        AlqSessionManager sessionManager = AlqSessionApi.provideAlqSessionManager();
        AlqSessionConnector sessionConnector = new AlqSessionConnector(sessionManager);
        RequestAuthenticatorService requestAuthenticatorService = new RequestAuthenticatorService(sessionConnector::getSessionOrEmpty);
        RequireSession requireSession = new RequireSession(requestAuthenticatorService);
        Spark.before(requireSession);

        Spark.before((req, res) -> res.type("application/json"));

        AlqLuckPermsGroupService groupService = new AlqLuckPermsGroupService(luckperms, luckpermsMapper);
        AlqGroupController groupController = new AlqGroupController(groupService);
        Spark.get("/perms/api/group", groupController::listGroups, AlqJson::serialize);
        Spark.get("/perms/api/group/:name", groupController::getGroup, AlqJson::serialize);
        Spark.put("/perms/api/group/:name", groupController::putGroup, AlqJson::serialize);
        Spark.delete("/perms/api/group/:name", groupController::deleteGroup, AlqJson::serialize);
        Spark.get("/perms/api/group/:name/inherited", groupController::getInheritedGroups, AlqJson::serialize);
        Spark.post("/perms/api/group/:name/inherited/add", groupController::addInheritedGroup, AlqJson::serialize);
        Spark.post("/perms/api/group/:name/inherited/remove", groupController::removeInheritedGroup, AlqJson::serialize);
        Spark.get("/perms/api/group/:name/permissions", groupController::listPermissions, AlqJson::serialize);
        Spark.post("/perms/api/group/:name/permissions/add", groupController::addPermission, AlqJson::serialize);
        Spark.post("/perms/api/group/:name/permissions/remove", groupController::removePermission, AlqJson::serialize);

        AlqLuckPermsUserService userService = new AlqLuckPermsUserService(luckperms, luckpermsMapper);
        AlqUserController userController = new AlqUserController(userService);
        Spark.get("/perms/api/user", userController::listUsers, AlqJson::serialize);
        Spark.get("/perms/api/user/:id", userController::getUser, AlqJson::serialize);
//        Spark.put("/perms/api/user/:id", userController::putUser, AlqJson::serialize);
        Spark.delete("/perms/api/user/:id", userController::deleteUser, AlqJson::serialize);
        Spark.get("/perms/api/user/:id/inherited", userController::getInheritedGroups, AlqJson::serialize);
        Spark.post("/perms/api/user/:id/inherited/add", userController::addInheritedGroup, AlqJson::serialize);
        Spark.post("/perms/api/user/:id/inherited/remove", userController::removeInheritedGroup, AlqJson::serialize);
        Spark.get("/perms/api/user/:id/permissions", userController::listPermissions, AlqJson::serialize);
        Spark.post("/perms/api/user/:id/permissions/add", userController::addPermission, AlqJson::serialize);
        Spark.post("/perms/api/user/:id/permissions/remove", userController::removePermission, AlqJson::serialize);
        Spark.get("/perms/api/user/:id/permissions/:permission/has", userController::hasPermission, AlqJson::serialize);

        AlqLuckPermsTrackService trackService = new AlqLuckPermsTrackService(luckperms, luckpermsMapper);
        AlqTrackController trackController = new AlqTrackController(trackService);
        Spark.get("/perms/api/track", trackController::listTracks, AlqJson::serialize);
        Spark.get("/perms/api/track/:name", trackController::getTrack, AlqJson::serialize);
        Spark.put("/perms/api/track/:name", trackController::putTrack, AlqJson::serialize);
        Spark.delete("/perms/api/track/:name", trackController::deleteTrack, AlqJson::serialize);
        Spark.post("/perms/api/track/:name/promote", trackController::promote, AlqJson::serialize);
        Spark.post("/perms/api/track/:name/demote", trackController::demote, AlqJson::serialize);

    }
}
