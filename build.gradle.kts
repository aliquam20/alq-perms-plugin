import org.aliquam.AlqUtils

plugins {
    `maven-publish`
    id("java")
    id("org.sonarqube") version "3.0"
    jacoco
    id("io.freefair.lombok") version "6.0.0-m2"
    id("org.aliquam.alq-gradle-parent") version "0.4.14"
}

repositories {
//    mavenCentral()
//    maven("https://jitpack.io")
    maven("https://papermc.io/repo/repository/maven-public/")
    mavenCentral()
}

val alq = AlqUtils(project).withStandardProjectSetup()

val artifactId = "alq-perms-plugin"
val baseVersion = "0.0.1"
group = "org.aliquam"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")

java.sourceCompatibility = JavaVersion.VERSION_11

sonarqube {
    if (branchName != null) {
        properties {
            property("sonar.projectKey", "aliquam20_${artifactId}")
            property("sonar.organization", "aliquam")
            property("sonar.host.url", "https://sonarcloud.io")
            property("sonar.branch.name", branchName)
        }
    }
}

val shadow = configurations.create("shadow")
dependencies {
    implementation("org.mapstruct:mapstruct:1.4.2.Final")
    annotationProcessor("org.mapstruct:mapstruct-processor:1.4.2.Final")
    annotationProcessor("org.projectlombok:lombok-mapstruct-binding:0.2.0")

    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.0") // Needed to use TypeReference<>
    implementation("org.aliquam:alq-perms-api:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-cum:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-cum-plugin:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-config-api:0.0.1-DEV_BUILD")
    implementation("org.aliquam:alq-session-api:0.0.1-DEV_BUILD")

    compileOnly("com.destroystokyo.paper:paper-api:1.16.5-R0.1-SNAPSHOT")
    compileOnly("net.luckperms:api:5.3")

//    implementation("io.javalin:javalin:4.1.1")
    implementation("com.sparkjava:spark-core:2.9.3")

    testImplementation("junit:junit:4.13.2")
    testImplementation("org.hamcrest:hamcrest-library:2.2")
}

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(arrayOf(
        // Tell MapStruct that we're using plain Java
        "-Amapstruct.defaultComponentModel=default",
        // I like constructors more than (default) field setters
        "-Amapstruct.defaultInjectionStrategy=constructor",
        // MapStruct will force us to explicitly exclude unmapped fields
        "-Amapstruct.unmappedTargetPolicy=ERROR"
    ))
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    from("plugin.yml")
    from(shadow.map { if (it.isDirectory) it else zipTree(it) })
//    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    duplicatesStrategy = org.gradle.api.file.DuplicatesStrategy.EXCLUDE
}

publishing {
    publications {
        create<MavenPublication>("myPublication") {
            groupId = "$group"
            artifactId = artifactId
            from(components["java"])
        }
    }
}
