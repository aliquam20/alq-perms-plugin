rootProject.name = "alq-perms-plugin"

pluginManagement {
    repositories {
        mavenLocal()
        maven("https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}